#!/bin/bash

cd "/construction-site"

generate_project () {
    # What should we call it?
    NEW_PROJECT_NAME=$(whiptail --inputbox "Give the project a name." 8 78 frontend --title "Application name" 3>&1 1>&2 2>&3)
    
    cd /construction-site/angular-app
    echo "Generating project. Naming it $NEW_PROJECT_NAME..."

    ng generate application --routing=true --style=sass $NEW_PROJECT_NAME
    wait
}

WORKSPACE_SET_UP=0
# Is anything here? Has the angular workspace been set up?

if [[ -d /construction-site/angular-app ]]
then
    # Workspace (root folder) is present.
    WORKSPACE_SET_UP=1

    if [[ -z "$(ls -A ./angular-app/)" ]]; then 
        echo "Application folder is here but it's empty. Generating app files..."; 
	ng new angular-app --create-application=false
	wait
    else 
        echo "An angular application is already set up. Files are present."
    fi
else
    if (whiptail --title "Set up new application?" --yesno "No angular application detected. Do you want to create one?" 8 78)
    then
	# What should we call it?
	APPLICATION_NAME=$(whiptail --inputbox "How should we name the app?" 8 78 angular-app --title "Application name" 3>&1 1>&2 2>&3)
        echo "Creating application. Naming it $APPLICATION_NAME..."
	ng new $APPLICATION_NAME --create-application=false
	wait
    else
        echo "Aborting project creation..."
    fi
fi

# Big-boss ROOT created the application.
# Let's allow everyone acces to the application so they can work on it.
chown -Rf 1000:1000 ./angular-app

# --------------------------------------------------------------
# Application. Check! Let's see if we have any projects created.
if [[ $WORKSPACE_SET_UP -eq 0 ]]; then exit 1; fi

# Check if project folders exists.
if [[ -d /construction-site/angular-app/projects/ ]]; then
    # GET all the applications/folders and store them.
    DIRS=()

    for FILE in /construction-site/angular-app/projects/*; do
	[[ -d $FILE ]] && DIRS+=("$(basename "$FILE")")
    done

    # Create the menu structure.
    MENU_OPTS=()
    for i in "${DIRS[@]}"
    do
        MENU_OPTS+=("$i") 
	MENU_OPTS+=("ng serve this project.")
    done
    
    # Add option to create a new project.
    MENU_OPTS+=( "GENERATE" "Generate a new project.")
    MENU_OPTS+=( "Exit" "Exit without making any changes.")

    MENU_RESULT="GENERATE"

    while [ "$MENU_RESULT" = "GENERATE" ]; do
        # Display the menu.
        MENU_RESULT=$(whiptail --title "What do you want to do?" --menu "Choose how you want to proceed:" 25 78 16 \
		"${MENU_OPTS[@]}" 3>&1 1>&2 2>&3)
        echo "You chose to $MENU_RESULT"

	if [ "$MENU_RESULT" = "Exit" ]; then
	    break
	fi

	if [ "$MENU_RESULT" = "GENERATE" ]; then
	    generate_project $MENU_RESULT 

	    unset 'MENU_OPTS[${#MENU_OPTS[@]}-1]'
	    unset 'MENU_OPTS[${#MENU_OPTS[@]}-1]'
	    unset 'MENU_OPTS[${#MENU_OPTS[@]}-1]'
	    unset 'MENU_OPTS[${#MENU_OPTS[@]}-1]'

	    MENU_OPTS+=("$i")
	    MENU_OPTS+=("ng serve this project.")

            # Add option to create a new project.
            MENU_OPTS+=( "GENERATE" "Generate a new project.")
            MENU_OPTS+=( "Exit" "Exit without making any changes.")
	fi
    done

    # What should we call it?
    else
        if (whiptail --title "Set up new project?" --yesno "There are no projects here. Set one up now?" 8 78); then
	    # What should we call it?
	    projectName=$(whiptail --inputbox "Let's give the application a name." 8 78 frontend --title "Application name" 3>&1 1>&2 2>&3)

	    cd /construction-site/angular-app
            echo "Creating application. Naming it $projectName..."
	    ng generate application --routing=true --style=sass $projectName
	    wait
	fi
fi

# Big-boss ROOT created the project.
# Let's allow everyone acces to the application so they can work on it.
chown -Rf 1000:1000 /construction-site/angular-app
	   
# Remember to also execute the command sent to us. 
exec "$@"
